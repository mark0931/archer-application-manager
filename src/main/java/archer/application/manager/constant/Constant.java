package archer.application.manager.constant;

/**
 * @author christ
 * @date 2016/6/12
 */
public interface Constant extends archer.framework.core.constant.Constant {

    String LOGIN_ERROR_INFO = "LOGIN_ERROR_INFO";
}
