package archer.application.manager.service;

import archer.framework.client.exception.RemoteInvokeException;
import archer.framework.protocol.result.ExecuteResult;
import archer.framework.protocol.result.ResultCode;

/**
 * @author christ
 * @date 2016/6/29
 */
public class RemoteInvokeService extends archer.framework.client.service.RemoteInvokeService {

    @Override
    public void resultHandle(ExecuteResult result) {

        super.resultHandle(result);

        if (ResultCode.INTERNAL_ERROR.name().equals(result.getResultCode())) {

            throw new RemoteInvokeException(ResultCode.INTERNAL_ERROR.getMsg());
        }

        if (ResultCode.NOT_LOGINED.name().equals(result.getResultCode())) {

            throwNotLoginedException();
        }
    }
}
