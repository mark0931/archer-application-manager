package archer.application.manager.controller.system;

import archer.framework.client.controller.UIController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author christ
 * @date 2016/6/21
 */
@Controller
@RequestMapping("manager/system/role")
public class RoleController extends UIController {

    @RequestMapping(method = RequestMethod.GET)
    public String index() {
        return "system/role";
    }

    @RequestMapping(value = "detail", method = RequestMethod.GET)
    public String roleDetail() {
        return "system/roleDetail";
    }
}
