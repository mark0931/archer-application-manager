package archer.application.manager.controller.system;

import archer.framework.client.controller.UIController;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author christ
 * @date 2016/6/12
 */
@Controller
@RequestMapping("manager/system/menu")
@Transactional
public class MenuController extends UIController {

    @RequestMapping(method = RequestMethod.GET)
    public String index() {
        return "system/menu";
    }

}
