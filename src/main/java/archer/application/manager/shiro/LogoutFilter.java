package archer.application.manager.shiro;

import archer.framework.protocol.result.ExecuteResult;
import archer.framework.utils.RemoteInvokeUtils;
import archer.framework.utils.ServletUtils;
import org.apache.shiro.session.SessionException;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * @author christ
 * @date 2016/6/14
 */
public class LogoutFilter extends org.apache.shiro.web.filter.authc.LogoutFilter {

    @Override
    protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {

        // 调用远程接口
        ExecuteResult result = RemoteInvokeUtils.post(ServletUtils.getRequest(request), ServletUtils.getResponse(response),
                "/core/internal/system/staff/logout");

        if (result.isFail()) {
            throw new SessionException();
        }

        return super.preHandle(request, response);
    }
}
