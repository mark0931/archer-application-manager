<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>用户管理-用户列表</title>
    <link rel="stylesheet" href="${ctx}/static/css/crud.css">
</head>
<body>

<div id="main" class="content-wrapper">

    <!-- header start -->
    <section class="content-header">

        <!-- header title start -->
        <h1>
            用户列表
            <small></small>
        </h1>
        <!-- header title end -->

        <!-- header breadcrumb start -->
        <ol class="breadcrumb">
            <li>
                <a href="#">系统管理</a>
            </li>
            <li class="active">
                用户管理
            </li>
        </ol>
        <!-- header breadcrumb end -->
    </section>
    <!-- header end -->

    <!-- main content start -->
    <section class="content">

        <div class="row">
            <div class="col-md-12">

                <!-- box start -->
                <div class="box box-success">

                    <!-- box header start -->
                    <div class="box-header">
                        <h2 class="box-title"></h2>

                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- box header end -->

                    <!-- box body start -->
                    <div class="box-body">

                        <!-- form start -->
                        <form class="search-form" id="searchForm">
                            <div class="condition with-padding">
                                <div class="row">
                                    <div class="col-xs-2">
                                        <input type="text" name="loginName" data-op="starts" class="form-control"
                                               placeholder="用户名">
                                    </div>
                                    <div class="col-xs-2">
                                        <input type="text" name="mobile" data-op="starts" class="form-control"
                                               placeholder="手机号">
                                    </div>
                                    <div>
                                        <button type="button" title="查询" class="btn btn-default btn-flat"
                                                onclick="query()">
                                            <i class="glyphicon glyphicon-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- form end -->

                        <div class="archer-table">
                            <!-- action start -->
                            <div class="table-action-bar">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default" onclick="insert();" title="新增">
                                        新增
                                    </button>
                                    <button type="button" class="btn btn-default" onclick="removeAll();" title="删除">
                                        删除
                                    </button>
                                </div>
                            </div>
                            <!-- action end -->

                            <!-- table start -->
                            <table id="masterTable"
                                   data-unique-id="id"
                                   data-query-params-type="archer"
                                   data-toolbar=".table-action-bar"
                                   data-toggle="table"
                                   data-ajax="load"
                                   data-side-pagination="server"
                                   data-click-to-select="true"
                                   data-show-refresh="true"
                                   data-show-columns="true"
                                   data-pagination="true">
                                <thead>
                                <tr>

                                    <th data-checkbox="true">
                                    </th>
                                    <th data-field="loginName"
                                        data-sortable="true">
                                        用户名
                                    </th>
                                    <th data-field="mobile">
                                        手机号
                                    </th>
                                    <th data-field="email">
                                        邮箱
                                    </th>
                                    <th data-field="status"
                                        data-formatter="statusRender">
                                        状态
                                    </th>
                                    <th data-field="sysData"
                                        data-formatter="booleanRender">
                                        系统数据
                                    </th>
                                    <th data-field="action"
                                        data-width="200px"
                                        data-align="center"
                                        data-formatter="inlineActionRender"
                                        data-events="inlineActions">
                                        操作
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- table end -->

                    </div>
                    <!-- box body start -->

                </div>
                <!-- box start -->
            </div>
        </div>

        <!-- modal start -->
        <div id="editor" class="edit-form fade modal" data-backdrop="static">
            <div class="modal-dialog" style="margin-top: 200px;width: 800px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">用户信息</h4>
                    </div>
                    <div class="modal-body">


                        <form class="form-horizontal">
                            <div class="box-body">

                                <div class="form-group">
                                    <label required for="name" class="col-sm-2 control-label">用户名</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="loginName" class="form-control" id="name"
                                               v-model="data.loginName">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="mobile" class="col-sm-2 control-label">手机号</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="mobile" class="form-control" id="mobile"
                                               v-model="data.mobile">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="email" class="col-sm-2 control-label">邮箱</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="email" class="form-control" id="email"
                                               v-model="data.email">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label required for="password" class="col-sm-2 control-label">密码</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="password" class="form-control" id="password"
                                               v-model="data.password">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label required for="status" class="col-sm-2 control-label">状态</label>

                                    <div class="col-sm-10">
                                        <select v-model="data.status" id="status" name="status"
                                                class="form-control select-enum"
                                                data-enum-key="UserStatus">

                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="box-footer">
                                <button type="button" class="btn btn-default btn-flat pull-right" onclick="store()">保存
                                </button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <!-- modal end -->
    </section>
    <!-- main content end -->
</div>

<script src="${ctx}/static/js/common/archer-crud.js"></script>
<script>

    //------------------------table methods--------------------------
    /**
     * 行操作界面
     */
    function inlineActionRender(value) {
        return [
            ' <a class="inline-operation inline-update">修改</a>',
            ' <a class="inline-operation inline-remove">删除</a>',
        ].join('');
    }

    function statusRender(value) {
        return $Enums.UserStatus[value];
    }

    function booleanRender(value) {
        return $Enums.BooleanStatus[value];
    }

    /**
     * 行操作事件
     */
    var inlineActions = {

        'click .inline-update': update,

        'click .inline-remove': remove
    };


    //------------------------crud--------------------------

    /**
     * 查询
     */
    function query() {
        masterComponent.reloadTableData();
    }

    /**
     * 新增
     */
    function insert() {
        masterComponent.insert({status:$Constants.enumKeys.ENABLE});
    }

    /**
     * 加载数据
     */
    function load(params) {
        masterComponent.loadTableData('${ctx}/core/internal/system/staff/findPagedList', params);
    }

    /**
     * 保存数据
     */
    function store() {
        masterComponent.store('${ctx}/core/internal/system/staff/store');
    }

    /**
     * 删除数据
     */
    function remove(e, value, row) {
        masterComponent.remove('${ctx}/core/internal/system/staff/delete', row);
    }

    /**
     * 删除多条数据
     */
    function removeAll() {
        var ids = masterComponent.getSelections("id");
        if (!archer.utils.isEmpty(ids)) {
            masterComponent.remove('${ctx}/core/internal/system/staff/deleteAll', {ids: ids.join(',')});
        }
    }

    function update(e, value, row) {
        masterComponent.update(row);

    }

    //------------------------ init --------------------------
    function initBefore(){
        $(".select-enum").initSelect();
    }

    //如果名称相同,可以直接写成_crud.init({});
    var masterComponent = _crud.createCrud({
        scope: '#main',                 //vue scope
        table: '#masterTable',
        editor: '#editor',
        searchForm: '#searchForm'
    });

    masterComponent.init(initBefore);

</script>
</body>
</html>
