<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <title>菜单管理-菜单列表</title>
    <link rel="stylesheet" href="${ctx}/static/css/crud.css">
</head>
<body>

<div id="main" class="content-wrapper">

    <!-- header start -->
    <section class="content-header">

        <!-- header title start -->
        <h1>
            菜单列表
            <small></small>
        </h1>
        <!-- header title end -->

        <!-- header breadcrumb start -->
        <ol class="breadcrumb">
            <li>
                <a href="#">系统管理</a>
            </li>
            <li class="active">
                菜单管理
            </li>
        </ol>
        <!-- header breadcrumb end -->
    </section>
    <!-- header end -->

    <!-- main content start -->
    <section class="content">

        <div class="row">
            <div class="col-md-12">

                <!-- box start -->
                <div class="box box-success">

                    <!-- box header start -->
                    <div class="box-header">
                        <h2 class="box-title"></h2>

                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- box header end -->

                    <!-- box body start -->
                    <div class="box-body">

                        <!-- form start -->
                        <!-- form end -->

                        <div class="result-table">
                            <!-- action start -->
                            <div class="table-action-bar">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default" onclick="insert();" title="新增">
                                        新增
                                    </button>
                                </div>
                            </div>
                            <!-- action end -->

                            <!-- table start -->
                            <table id="masterTable"
                                   data-unique-id="id"
                                   data-query-params-type="archer"
                                   data-toolbar=".table-action-bar"
                                   data-toggle="table"
                                   data-ajax="load"
                                   data-show-refresh="true"
                                   data-show-columns="true"
                                    >
                                <thead>
                                <tr>
                                    <th data-field="sort">
                                        序号
                                    </th>
                                    <th data-field="name"
                                        data-formatter="nameRender">
                                        名称
                                    </th>
                                    <th data-field="url">
                                        地址
                                    </th>

                                    <th data-field="display"
                                        data-formatter="displayRender">
                                        显示
                                    </th>
                                    <th data-field="remark">
                                        备注
                                    </th>
                                    <th data-field="action"
                                        data-width="200px"
                                        data-align="center"
                                        data-formatter="inlineActionRender"
                                        data-events="inlineActions">
                                        操作
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- table end -->

                    </div>
                    <!-- box body start -->

                </div>
                <!-- box start -->
            </div>
        </div>

        <!-- modal start -->
        <div id="editor" class="edit-form fade modal" data-backdrop="static">
            <div class="modal-dialog" style="margin-top: 200px;width: 800px">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">菜单信息</h4>
                    </div>
                    <div class="modal-body">


                        <form class="form-horizontal">
                            <div class="box-body">

                                <div class="form-group">
                                    <label for="parentId" class="col-sm-2 control-label">上级菜单</label>

                                    <div class="col-sm-10">
                                        <select class="form-control " name="parentId" id="parentId"
                                                v-model="data.parentId" style="width: 100%">
                                            <option v-for="item in other" value="{{item.id}}"
                                                    selected="{{item.selected}}">
                                                {{item.name}}
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label required for="name" class="col-sm-2 control-label">名称</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="name" class="form-control" id="name"
                                               v-model="data.name">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label required for="url" class="col-sm-2 control-label">地址</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="url" class="form-control" id="url"
                                               v-model="data.url">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label required for="display" class="col-sm-2 control-label">显示</label>

                                    <div class="col-sm-10">
                                        <select v-model="data.display" id="display" name="display"
                                                class="form-control select-enum"
                                                data-enum-key="BooleanStatus">

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label required for="sort" class="col-sm-2 control-label">序号</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="sort" class="form-control" id="sort"
                                               v-model="data.sort">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="remark" class="col-sm-2 control-label">备注</label>

                                    <div class="col-sm-10">
                                        <input type="text" name="remark" class="form-control" id="remark"
                                               v-model="data.remark">
                                    </div>
                                </div>

                            </div>
                            <div class="box-footer">
                                <button type="button" class="btn btn-default btn-flat pull-right" onclick="store()">保存
                                </button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        <!-- modal end -->
    </section>
    <!-- main content end -->
</div>

<script src="${ctx}/static/js/common/archer-crud.js"></script>
<script>

    //------------------------table methods--------------------------
    /**
     * 行操作界面
     */
    function inlineActionRender(value) {
        return [
            ' <a class="inline-operation inline-update">修改</a>',
            ' <a class="inline-operation inline-remove">删除</a>',
        ].join('');
    }


    /**
     * 行操作事件
     */
    var inlineActions = {

        'click .inline-update': update,

        'click .inline-remove': remove
    };

    /**
     * 名称列渲染器
     */
    function nameRender(value, row) {
        if (archer.utils.isEmpty(row.parentId)) {
            return row.name;
        }
        return "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;∟" + row.name;
    }

    function displayRender(value, row) {
        return $Enums.BooleanStatus[row.display];
    }
    //------------------------crud--------------------------

    /**
     * 新增数据
     */
    function insert() {

        masterComponent.insert({display:$Constants.enumKeys.Y});
    }


    /**
     * 加载数据
     */
    function load(params) {
        masterComponent.loadTableData('${ctx}/core/internal/system/menu/findList', params, function (data) {
            initSelect(data);
        });
    }

    /**
     * 保存数据
     */
    function store() {
        masterComponent.store('${ctx}/core/internal/system/menu/store');
    }

    /**
     * 删除数据
     */
    function remove(e, value, row) {
        masterComponent.remove('${ctx}/core/internal/system/menu/delete', row);
    }

    /**
     * 初始化下拉框
     */
    function initSelect(data) {
        if (archer.utils.isSuccess(data) && !archer.utils.isEmpty(data.resultData)) {
            var tree = [];
            for (var i = 0; i < data.resultData.length; i++) {
                if (archer.utils.isEmpty(data.resultData[i].parentId)) {
                    tree.push(_.cloneDeep(data.resultData[i]))
                }
            }
            masterComponent.vm.other = tree;
        }
    }

    /**
     * 修改数据
     */
    function update(e, value, row) {

        masterComponent.update(row);
    }

    //------------------------ init --------------------------
    function initBefore(){
        $(".select-enum").initSelect();
    }

    var masterComponent = _crud.createCrud({
        scope: '#main',                 //vue scope
        table: '#masterTable',
        editor: '#editor',
    });

    masterComponent.init(initBefore);

</script>
</body>
</html>
