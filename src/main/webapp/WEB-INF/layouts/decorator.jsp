<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="sitemesh" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="maximum-scale=1.0,minimum-scale=1.0,user-scalable=0,width=device-width,initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title><sitemesh:title/></title>

    <link rel="stylesheet" href="${ctx}/static/vendor/bootstrap/css/bootstrap.min.css">
    <link href="//cdn.bootcss.com/bootstrap-table/1.11.0/bootstrap-table.min.css" rel="stylesheet">

    <link rel="stylesheet" href="${ctx}/static/vendor/AdminLTE/css/AdminLTE.css">
    <link rel="stylesheet" href="${ctx}/static/vendor/AdminLTE/css/skins/_all-skins.css">

    <link rel="stylesheet" href="${ctx}/static/vendor/messenger/css/messenger.css">
    <link rel="stylesheet" href="${ctx}/static/vendor/messenger/css/messenger-theme-flat.css">
    <link rel="stylesheet" href="${ctx}/static/vendor/jquery-confirm/jquery-confirm.min.css">
    <style>
        /**菜单*/
        .skin-green-light .sidebar-menu > li > a {
            border-left: 3px solid transparent;
            font-weight: normal;
        }

        .skin-green-light .sidebar-menu > li.active > a {
            font-weight: normal;
        }

        .sidebar-menu .treeview-menu > li > a {
            padding: 5px 5px 5px 35px;
            display: block;
            font-size: 14px;
        }

        .sidebar-menu > li.active > a {
            color: #fff;
            background: #1e282c;
        }

        .treeview-menu > li.active > a {
            border-right: 3px solid #00a65a;
        }

        .treeview-menu > li.active > a > span {
            color: #00a65a;
            font-weight: normal;
        }
    </style>

    <link href="//cdn.bootcss.com/font-awesome/4.6.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="//cdn.bootcss.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">


    <script src="${ctx}/static/vendor/jquery/jquery.min.js"></script>
    <script src="//cdn.bootcss.com/lodash.js/4.12.0/lodash.js"></script>
    <script src="//cdn.bootcss.com/vue/1.0.24/vue.min.js"></script>
    <script src="${ctx}/static/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="${ctx}/static/vendor/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="${ctx}/static/vendor/AdminLTE/js/app.js"></script>
    <script src="${ctx}/static/vendor/fastclick/fastclick.js"></script>

    <script src="${ctx}/static/vendor/messenger/js/messenger.min.js"></script>
    <script src="${ctx}/static/vendor/messenger/js/messenger-theme-flat.js"></script>
    <script src="${ctx}/static/vendor/jquery-confirm/jquery-confirm.min.js"></script>

    <script src="//cdn.bootcss.com/bootstrap-table/1.11.0/bootstrap-table.min.js"></script>
    <script src="${ctx}/static/vendor/bootstrap-table/bootstrap-table-zh.js"></script>
    <script src="${ctx}/static/vendor/url/purl.js"></script>

    <script src="${ctx}/static/js/common/constants.js"></script>
    <script src="${ctx}/static/js/common/archer.js"></script>

    <sitemesh:head/>
</head>

<body class="hold-transition skin-green-light sidebar-mini ">
<div class="wrapper">


    <%@include file="/WEB-INF/layouts/header.jsp" %>

    <sitemesh:body/>


    <script>

        // messager初始化
        Messenger.options = {
            extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-right',
            theme: 'flat'
        };
    </script>
</div>
</body>

</html>