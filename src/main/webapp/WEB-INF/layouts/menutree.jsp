<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>

<c:forEach var="menu" items="${menuData}">
    <c:choose>
        <c:when test="${not empty menu.children}">
            <li id="${menu.id}" class="treeview">
                <a href="${ctx}${menu.url}">
                    <i class="${menu.iconClass}"></i>
                    <span>${menu.name}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <c:set var="menuData" value="${menu.children}" scope="request"/>
                    <jsp:include page="menutree.jsp"/>
                </ul>
            </li>
        </c:when>
        <c:otherwise>
            <li id="${menu.id}" onclick="selectNode('${menu.id}')">
                <a href="${ctx}${menu.url}">
                    <i class="${menu.iconClass}"></i>
                    <span>${menu.name}</span>
                </a>
            </li>
        </c:otherwise>
    </c:choose>
</c:forEach>
